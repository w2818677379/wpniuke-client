import Vue from 'vue'
import Router from 'vue-router'
import house_index from '@/components/house/index'
import index from '@/components/index'
import resold_House from "../components/house/resold_House";
import centerIndex from "../components/userCenter/centerIndex";
import resoldHouseDetail from "../components/house/resoldHouseDetail";
import broker from "../components/broker/broker";
import empty from "../components/house/empty";
import login from '@/components/login'
import register from '@/components/register'
import pay from "../components/order/pay";
import house_rent_detail from "@/components/house/house_rent_detail";
import broker_admin from "@/components/broker/broker_admin";
import newHouseInfo from "../components/userCenter/newHouseInfo";
import residence from "../components/residen/residence";

Vue.use(Router)



export default new Router({
  routes: [

    {
      path: '/broker/broker_admin',
      name: 'broker_admin',
      component: broker_admin
    },

    {
      path: '/house/house_rent_detail',
      name: 'house_rent_detail',
      component: house_rent_detail
    },

    {
      path: '/house/empty',
      name: 'empty',
      component: empty
    },

    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/house/index',
      name: 'house_index',
      component: house_index
    },
    {
      path: '/house/resold_House',
      name: 'resold_House',
      component: resold_House
    },
    {
      path: '/residen/residence',
      name: 'residence',
      component: residence
    },
    {
      path: '/userCenter/centerIndex',
      name: 'centerIndex',
      component: centerIndex
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/order/pay',
      name: 'pay',
      component: pay
    },
    {
      path: '/house/resoldHouseDetail',
      name: 'resoldHouseDetail',
      component: resoldHouseDetail
    },
    {
      path: '/broker/broker',
      name: 'broker',
      component: broker
    },
    {
      path:'/usercenter/newHouseInfo',
      name:'newHouseInfo',
      component: newHouseInfo
    }
  ]
})
