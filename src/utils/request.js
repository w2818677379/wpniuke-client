import axios from 'axios'
//第一步引入axios
//引入token
import {getToken}from '@/utils/auth'


//引入mui和router
// import '../../static/mui/css/mui.css'
// import mui from '../../static/mui/js/mui.js'
//引入路由

import router from '../router/index.js'
import Message from "element-ui/packages/message/src/main";



//第二部创建一个axios实例
//常量不可以被修改，但是常量如果是对象可以修改对象内的属性
const service = axios.create({
  baseURL: 'http://127.0.0.1:5050', //根路径
  timeout: '10000' //连接超时时间
})

//请求发送前的处理
service.interceptors.request.use(request => {
  console.log(request)
  //1.从浏览器的cookie中获取token
  let token = getToken()

  //2.将token存入请求头中
  request.headers['token'] = token
  return request
}, error => {
  console.log(error)
  return Promise.reject(error)
})



//请求发送后的处理
service.interceptors.response.use(response=>{
  console.log(response)
  //如果状态码是500给个提示然后跳转到登录页面
  if(response.data.code ==50003){
    console.log(response.data.code)
    Message.error("请先登录")
    router.push('/login')
  }else if (response.data.code ==50005){
    console.log(response.data.code)
    Message.error("登录信息已过期")
    router.push('/login')
  }else if (response.data.code ==50000){
    Message.error(response.data.msg)
  }else if (response.data.code == 52000){
    Message.warning(response.data.msg)
  }

  return response.data
},error=>{
  console.log(error)
  Message.error(error)
  return Promise.reject(error)
})

//导出service
export default service
