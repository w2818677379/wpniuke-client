import Cookies from 'js-cookie'

//定义token的键名
const TokenKey = 'token'

//定义三个方法
//获取token
export function getToken(){
  return Cookies.get(TokenKey)
}
// 设置token到cookie的方法
export function setToken(token){
  Cookies.set(TokenKey,token)
}
// 删除token的方法
export function removeToken(){
  Cookies.remove(TokenKey)
}
