//导入工具类
import request from '@/utils/request'

export function gotoPay(data){
  return request({
    url:'/orders/orders/gotoPay',
    method:'get',
    params:data
  })
}




export function getOrderStatus(data){
  return request({
    url:'/orders/orders/getOrderStatus',
    method:'get',
    params:data
  })
}

export function getMyContract(data){
  return request({
    url:'/orders/contract/getContractList',
    method:'post',
    data
  })
}

export function getMyOrders(data){
  return request({
    url:'/orders/orders/getOrderListByUserId',
    method:'get',
    params:data
  })
}

export function appkyToRefundMoney(data){
  return request({
    url:'/orders/orders/appkyToRefundMoney',
    method:'post',
    data
  })
}

export function getBrokerOrderByBroderId(data){
  return request({
    url:'/orders/orders/getBrokerOrderByBroderId',
    method:'get',
    params:data
  })
}

export function brokerRefundService(data){
  return request({
    url:'/orders/orders/brokerRefundService',
    method:'post',
    data
  })
}

export  function selectContractByBrokerId(data){
  return request({
    url:'/orders/contract/getContractByBrokerId',
    method: 'get',
    params: data
  })
}


export  function createNewLifeOrder(data){
  return request({
    url:'/orders/orders/createNewLifeOrder',
    method: 'post',
    data
  })
}
