import request from '@/utils/request'

export function loginByPassWord(data) {
  return request({
    url:"/user/user/loginByPassword",
    method:"post",
    data
  })
}

export function register(data) {
  return request({
    url:"/user/user/addUser",
    method:"post",
    data
  })
}

export function loginByCode(data) {
  return request({
    url:"/user/user/loginByCode",
    method:"post",
    data
  })
}

export function sendEmail(data) {
  return request({
    url:"/user/user/sendEmail",
    method:"post",
    data
  })
}

export function sendSMS(data) {
  return request({
    url:"/user/user/sendSMS",
    method:"post",
    data
  })
}

export function checkEmailInputCode(data) {
  return request({
    url:"/user/user/checkEmailInputCode",
    method:"post",
    data
  })
}

export function checkPhoneInputCode(data) {
  return request({
    url:"/user/user/checkPhoneInputCode",
    method:"post",
    data
  })
}

export function checkUser(data) {
  return request({
    url:"/user/user/checkUser",
    method:"post",
    data
  })
}

export function getUserInfo() {
  return request({
    url:"/user/user/getUserInfo",
    method:"post",
  })
}

export function changeInfoNeedToVerify(data){
  return request({
    url:"/user/user/changeInfoNeedToVerify",
    method:"post",
    data
  })
}

export function updateUser(data){
  return request({
    url:"/user/user/update",
    method:"post",
    data
  })
}

export function addHistory(data){
  return request({
    url:"/user/history/addHistory",
    method:"post",
    data
  })
}
export function getMyHistory(data){
  return request({
    url:"/user/history/getMyHistory",
    method:"get",
    params:data
  })
}


