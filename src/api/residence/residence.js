import request from '@/utils/request'

export function getResidenceAll(data){
  return request ({
    url:'/resolehouse/residence/selectAll',
    method:'post',
    data
  })
}

export function getAllById(data) {
  return request({
    url:'/resolehouse/resoldhouse/getAllById',
    method:'post',
    data
  })
}
