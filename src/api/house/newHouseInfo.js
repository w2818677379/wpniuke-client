import request from '@/utils/request'

export function addNewHouse(data) {
  return request({
    url:"/user/newhouseinfo/createNewHouseInfo",
    method:"post",
    data
  })
}

export function queryHouseInfo(data){
  return request({
    url:"/user/newhouseinfo/queryHouseInfoByUserId",
    method:"post",
    data
  })
}

export function updateHouseInfo(data){
  return request({
    url:"/user/newhouseinfo/updateHouseInfo",
    method:"post",
    data
  })
}

export function deleteHouseInfo(data){
  return request({
    url:"/user/newhouseinfo/deleteHouseInfo",
    method:"post",
    data
  })
}
