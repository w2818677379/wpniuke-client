import request from '@/utils/request'

export function getAddressList(){
  return request({
    url:'/house/address/getAddressList',
    method:'post'
  })
}

export function selectAddressChild(data){
  return request({
    url:'/house/address/selectAddressChild',
    method:'post',
    data
  })
}


export function selectAddressChildByName(data){
  return request({
    url:'/house/address/selectAddressChildByName',
    method:'post',
    data
  })
}

export function selectAddressParent(data){
  return request({
    url:'/house/address/selectAddressParent',
    method:'post',
    data
  })
}

export function getAllAddress(){
  return request({
    url:"/house/address/getAllAddress",
    method:"post"
  })
}



