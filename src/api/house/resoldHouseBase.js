import request from '@/utils/request'

export function getResoldHouseBaseByResoldId(data) {
  return request({
    url:'/resolehouse/resoldhousebase/GetHouseBaseByResoldHoouseId',
    method:'get',
    params: {data}
  })
}
