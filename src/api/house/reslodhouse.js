 import request from '@/utils/request'

export function getAllByResoldHouseId(data) {
  return request({
    url:'/resolehouse/resoldhousedetail/getAllByResoldHouseId',
    method:'get',
    params: data
  })
}
export function queryAllresoldHouse(data) {
  return request({
    url:'/resolehouse/resoldhouse/getHouseAndResidence',
    method:'post',
    data
  })
}
export function GetEvaluationAndBrokerById(data) {
  return request({
    url:'/resolehouse/resoldhouse/GetEvaluationAndBrokerById',
    method:'post',
    data
  })
}
export function selectAll(data) {
  return request({
    url: '/resolehouse/residence/selectAll',
    method:'post',
    data
  })
}
