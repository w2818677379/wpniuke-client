import request from '@/utils/request'


export function getHouseRentList(data){
  return request({
    url:'/house/houseRent/getHouseRentList',
    method:'post',
    data
  })
}


export function selectButton(data){
  return request({
    url:'/house/houseRent/selectButton',
    method:'post',
    data
  })
}

export function getHouseRentByApartmentId(data){
  return request({
    url:'/house/room_detail/getHouseRentByApartmentId',
    method:'post',
    data
  })
}

export function makeAnAppointment(data){
  return request({
    url:'/house/room_detail/makeAnAppointment',
    method:'post',
    data
  })
}

