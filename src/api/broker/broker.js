import request from '@/utils/request'

export function getBrokerAll(){
  return request({
    url:'/broker/broker/getBrokerAll',
    method:'post'
  })
}

export function selectBrokerServiceByUserId(data){
  return request({
    url:'/broker/broker/selectBrokerServiceByUserId',
    method:'get',
    params: data
  })
}

export function getBrokerOrderByBroderId(data){
  return request({
    url:'/broker/broker/getBrokerOrderByBroderId',
    method:'get',
    params: data
  })
}



