import request from '@/utils/request'

export function getBrokerAndEvaluation(data){
  return request({
    url:'/broker/evaluation/GetevaluationAndBrokerByResoldHouseId',
    method:'post',
    data
  })
}
